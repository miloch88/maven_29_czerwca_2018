package pl.sda;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class MainZapis {
    public static void main(String[] args) {

        try {
            PrintWriter writer = new PrintWriter("plik.txt");
            writer.println("Hello!");
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


//        try(PrintWriter writer = new PrintWriter("plik.txt")){
//            writer.println("Dupa!");
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }


        try(PrintWriter writer = new PrintWriter(new FileWriter("plik.txt", true))){
            writer.println("Dupa!");
            writer.flush();// Zapisuje w tym miejscu, zwalnia bufor

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
