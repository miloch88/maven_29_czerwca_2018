package pl.sda.zapis_zad5;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        try (PrintWriter writer = new PrintWriter(new FileWriter("output_form.txt", true))) {

            String wybor;

            do {

                String pytanie = "Podaj swój wiek: ";
                System.out.println(pytanie);
                writer.println(pytanie);
                String odpowiedz = scanner.nextLine();
                writer.println("Odpowiedz: " + odpowiedz);

                pytanie = "Podaj swój wzrost: ";
                System.out.println(pytanie);
                writer.println(pytanie);
                odpowiedz = scanner.nextLine();
                writer.println("Odpowiedz: " + odpowiedz);

                pytanie = "Podaj swoją płeć: ";
                System.out.println(pytanie);
                writer.println(pytanie);
                odpowiedz = scanner.nextLine();
                writer.println("Odpowiedz: " + odpowiedz);

                if(odpowiedz.toLowerCase().equals("kobieta")){
                    pytanie = "Jakie jest Twoje ulubione zwierzę: ";
                    System.out.println(pytanie);
                    writer.println(pytanie);
                    odpowiedz = scanner.nextLine();
                    writer.println("Odpowiedz: " + odpowiedz);
                }else{
                    pytanie = "Twój wymarzony samochód: ";
                    System.out.println(pytanie);
                    writer.println(pytanie);
                    odpowiedz= scanner.nextLine();
                    writer.println("Odpowiedz: " + odpowiedz);
                }

                pytanie = "Podaj swoje zarobki: ";
                System.out.println(pytanie);
                writer.println(pytanie);
                odpowiedz = scanner.nextLine();
                writer.println("Odpowiedz: " + odpowiedz);

                System.out.println();

                System.out.println("Czy chcesz kontynuuować?");
                wybor = scanner.nextLine();

            } while (!wybor.equals("nie"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
