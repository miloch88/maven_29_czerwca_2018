package pl.sda.zapis_zad4;

import java.io.File;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj adres pliku: ");
        String tekst = scanner.nextLine();

        File plik = new File(tekst);

        // Czy nie powienien wystąpić błąd, bo go nie obsługuję ???

//        if (plik.exists()) {
            System.out.println("Plik " + plik + " istnieje");
            System.out.println("Wielkość pliku: " + plik.length());
            System.out.println("Czas ostatniej modyfikacji: " + plik.lastModified());
            System.out.println("Czy jest folderem: " + plik.isDirectory());
            System.out.println("Czy jest plikiem: " + plik.isFile());
            System.out.println("Ścieżka relatywna: " + plik.getPath());
            System.out.println("Ścieżka absolutna: " + plik.getAbsolutePath());
            System.out.println("Czy mamy prawo do odczytu: " + plik.canRead() +
                    "\nCzy mamy prawo do zapisu " + plik.canWrite());
//        }
//        else {
//            System.out.println("Plik lub folder nie istnieje");
//        }
    }
}
