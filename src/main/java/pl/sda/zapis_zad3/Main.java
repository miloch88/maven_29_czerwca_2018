package pl.sda.zapis_zad3;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String wybor;

        try(PrintWriter writer = new PrintWriter(new FileWriter("output_3.txt", true))) {
            do{
                wybor = scanner.nextLine();
                writer.println(wybor);
                writer.flush();

            }while(!wybor.equals("quit"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
